package com.example.hacked

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hackedButton: Button = findViewById(R.id.button)
        hackedButton.setOnClickListener {
            Select()
        }
    }

    private fun Select() {
        val selectionClass = Selection()
        val selected = selectionClass.select()
        val resultTextView: TextView = findViewById(R.id.textView)
        resultTextView.text = selected.toString()

        val constrainedLayout: ConstraintLayout = findViewById(R.id.cl)
        if (selected == "Yes") {
            constrainedLayout.setBackgroundColor(Color.parseColor("#F44336"))
        }
        else if (selected == "No") {
            constrainedLayout.setBackgroundColor(Color.parseColor("#4CAF50"))
        }
        else {
            constrainedLayout.setBackgroundColor(Color.parseColor("#FFEB3B"))
        }
    }
}
